# IGwatch
## What it is

```igwatch``` is a small shell wrapper script, that uses the fantastic ```instagram-scraper``` to download posts from Instagram users and preview them right in your terminal.

![](demo.png)

## What you need

Mandatory:
- [Instagram-Scraper](https://github.com/rarcega/instagram-scraper): To scrape posts and stories of users.
- [FSwatch](https://github.com/emcrisostomo/fswatch): To monitor if a new image or story was downloaded.
- [PIXterm](https://github.com/eliukblau/pixterm): To view images in your terminal.

Install them according to their instructions. The script will try to check, if these three programs are installed on your system.

Alternatively:

- [TerminalImageViewer](https://github.com/stefanhaustein/TerminalImageViewer) if your terminal only supports 256 colors. Can also be used in true color. There will be **NO** visual difference to pixterm, but it lacks the ASCII character option. For other alternatives see [this AskUbuntu question](https://askubuntu.com/questions/97542/how-do-i-make-my-terminal-display-graphical-pictures).

Recommended: 

- [tmux](https://tmux.github.io/) or any other terminal multiplexer (e.g. [GNU Screen](https://www.gnu.org/software/screen/))
- A terminal that supports True Color, e.g. iTerm, xterm, PuTTY etc. For an overview this GIST: [Colours in terminal](https://gist.github.com/XVilka/8346728)

## How to install and use

The easy way

```sh
wget "https://gitlab.com/thnatiw/igwatch/raw/master/igwatch"
sh igwatch
```

or make it executable

```sh
wget "https://gitlab.com/thnatiw/igwatch/raw/master/igwatch"
chmod +rx igwatch
./igwatch
```

To stop the script simply press ```ctrl-c```.

The script uses a config file in the same directory as the script. (```igwatch.cfg```) 
If it is not present it will ask you a few questions and will save the configuration file if you choose to. 

You can edit this file in your favorite text editor at any time or delete the .cfg file in order to create a new configuration.

Name | Function
---|---
igusers | Instagram usernames to scraper (sperated by commas); e.g. instagram,natgeo,nasa
igdir | Directory to save downloads to; e.g. /home/user/instagram/
username | Your Instagram Username
password | Your Instagram Password (will not be shown)
wait | Wait-time between scrapes; in minutes (Use a value more then 15 minutes, as Instagram limits download requests per hour)
viewmode | Viewmode of downloaded pictures (Pixel-like or ASCII characters)

A logfile called ```igwatch.log``` will also be created on the first run. It simply logs when ```instagram-scraper``` ran sucessfully or if any errors occured. Errors will also be shown on screen. (e.g. failed logins or downloads)

This script is supposed to run indefinitely. So use this on a server or an always-on-computer (e.g. a Raspberry Pi) and inside a tmux session.